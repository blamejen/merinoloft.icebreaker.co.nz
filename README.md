# Task: Icebreaker Merino Loft Innovation

## Staging URL
[http://merinoloft.icebreaker.co.nz.dev.blamejen.net/](http://merinoloft.icebreaker.co.nz.dev.blamejen.net/)

## Installing packages
`npm install`
`yarn install`

## Webpack dev server
`npm start`

## Develpment build
`npm run build`

## Production build
`npm run build:prod`

# Project guide

## Steps
1. Initialise project with boilerplate
2. Configure local ENV: 'local.merinoloft.icebreaker.co.nz:3005'
3. Create a repository in Bitbucket
4. Set up project assets (compressed favicon, images, fonts, icons)
5. Make a set of CSS variables and mixin - color pallete, fonts, icons
6. Set HTML Header META tags
7. Build HTML Markup & CSS styling
8. Tidy up npm packages
9. Crossbrowser test
10. Site performance check

## Addition to the page
- Sticky header and footer
- Intro animation and scroll to animation - can be clicked or can scroll to down
- Text animation on the scroll
- Hover state for links
- Responsive layout

## Duration
5.5 hours to build

## Framework
CSS Framework: [Bootstrap 4](https://getbootstrap.com/)

Module Bundler: [Webpack 4](https://webpack.js.org/)

Vendors: [NPM](https://www.npmjs.com/)

-   HTML: HTML5
-   CSS:  Used SASS, BEM Methodology
-   JS:   Converted ECMAScript 2015+ code into a backwards compatible version of JavaScript in current and older browsers or environments using Babel

## Image compression
[TinyPNG](https://tinypng.com/) - Favicon, Any PNG images

[TinyJPG](https://tinyjpg.com/) - Any JPG images

## Fonts for icebreaker: Sofia Pro
Generated font-face kit as I cannot use production typekit account for my DEV env

file: client/fonts

## Icons for Icebreaker
Generated icebreaker icon set via [icomoon.io](https://icomoon.io/app/)

JSON file: /client/ico/selection.json

## Crossbrowser Testing
IE: IE10, IE11 & Edge

Safari: 12.0 (12606.2.11)

Chrome: 69.0.3497.100

Firefox: 62.0

## Site Performance
[PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/)

[W3C Markup Validation](https://validator.w3.org/)

