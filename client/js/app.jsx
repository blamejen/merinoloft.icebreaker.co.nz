import retina from 'retinajs';
window.addEventListener('load', retina);

import ScrollTo from './components/smoothscroll';
import Carousel from './components/carousel';
import MatchHeight from './components/matchheight';
import StickyNav from './components/stickynav';
import ScrollAnim from './components/scrollreveal';

new ScrollTo();
new MatchHeight();
new StickyNav();
new ScrollAnim();

new Carousel({
  selector: '.carousel'
}, {
  arrows: true,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  nextArrow: '<i class="icomn-arrow-right slick-next"></i>',
  prevArrow: '<i class="icomn-arrow-left slick-prev"></i>',
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 960,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

