import 'slick-carousel';

export default class Carousel {
  constructor(userOptions = {}, slickOptions = {}) {
    var options = Object.assign({
      selector: '.slick-carousel',
      slickOptions: Object.assign({
        autoPlay: false,
      }, slickOptions)
    }, userOptions);
    $(options.selector).slick(options.slickOptions);
  }
}
