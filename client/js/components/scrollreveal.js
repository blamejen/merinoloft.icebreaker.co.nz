import ScrollReveal from 'scrollreveal';

export default class ScrollAnim {
  constructor() {
    var config1 = {
      viewFactor : 0.15,
      duration   : 1000,
      distance   : '30px',
      origin: 'bottom',
      opacity: 0.5,
      reset: true,
      delay: 200
    };

    var text = {
      viewFactor : 0.15,
      duration   : 1500,
      distance   : 0,
      origin: 'bottom',
      opacity: 0,
      scale: 0.6,
      reset: true,
      delay: 200
    };

    var text2 = {
      viewFactor : 0.15,
      duration   : 1000,
      distance   : '100px',
      origin: 'left',
      opacity: 0,
      scale: 1,
      reset: true,
      delay: 100
    };

    var sr = ScrollReveal();
    sr.reveal('.sc-reveal', config1);
    sr.reveal('.sc-text', text);
    sr.reveal('.sc-text2', text2);
  }
}
