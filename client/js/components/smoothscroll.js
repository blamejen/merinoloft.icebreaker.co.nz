import SmoothScroll from 'smooth-scroll';

export default class ScrollTo {
    constructor() {
      new SmoothScroll('a[href*="#"]',{
        speed: 1000,
      });
    }
}
