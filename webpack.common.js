const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebPackPlugin = require("html-webpack-plugin");

let PATHS = {
  app: path.resolve(__dirname, 'client'),
  dist: path.resolve(__dirname, 'dist')
};

module.exports = {
  context: PATHS.app,
  entry: {
    common: './js/app.jsx',
    appStyles: [
      './styles/app.scss',
    ],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: function () { // post css plugins, can be exported to postcss.config.js
                return [
                  require('precss'),
                  require('autoprefixer')
                ];
              }
            }
          },
          'sass-loader'
        ],
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              name: 'css/[hash:8].[name].[ext]',
              mimetype: 'application/font-woff'
            }
          }
        ]
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              name: 'css/[hash:8].[name].[ext]',
              mimetype: 'application/octet-stream'
            }
          }
        ]
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              limit: 8192,
              name: 'css/[hash:8].[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              name: 'css/[hash:8].[name].[ext]',
              mimetype: 'mage/svg+xml'
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              name: 'css/[hash:8].[name].[ext]',
            }
          }
        ]
      },
      {
        test: /\.twig$/,
        loader: "twig-loader",
        options: { minimize: true }
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new CopyWebpackPlugin([
      { from: PATHS.app + '/{fav,img}/**/*', to: PATHS.dist },
      { from: PATHS.app + '/misc', to: PATHS.dist },
    ], {
      copyUnmodified: true
    }),
    new HtmlWebPackPlugin({
      filename: './index.html',
      template: './templates/pages/index.twig',
      minify: {
        removeScriptTypeAttributes: true,
      },
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      Popper: ['popper.js', 'default'],
      // In case you imported plugins individually, you must also require them here:
      Util: "exports-loader?Util!bootstrap/js/dist/util",
      Collapse: "exports-loader?Modal!bootstrap/js/dist/collapse"
    })
  ]
};
