const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

let PATHS = {
  app: path.resolve(__dirname, 'client'),
  dist: path.resolve(__dirname, 'dist')
};

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: PATHS.app,
    host: "local.merinoloft.icebreaker.co.nz",
    port: 3005
  },
  output: {
    path: PATHS.dist,
    publicPath: '/',
    filename: 'js/[name].js',
    chunkFilename: 'js/[name].js',
  },
});
